#android-dimension-converter

Dimension Converter

##Features
* Conversion in dp、dip、sp、pt、px、mm、in according your device
* Conversion in ldpi、mdpi、tvdpi、hdpi、xhdpi、xxhdpi、xxxhdpi

##Requirements
Android 2.2 +

##Release
[Github](https://github.com/snowdream/android-dimension-converter/releases/download/0.0.1/dimension-converter-v0.0.1-release.apk)

##Preview
![dimension.png](https://raw.githubusercontent.com/snowdream/android-dimension-converter/master/docs/preview/dimension.png "dimension.png")

![dpi.png](https://raw.githubusercontent.com/snowdream/android-dimension-converter/master/docs/preview/dpi.png "dpi.png")

##License
```
Copyright (C) 2014 Snowdream Mobile <yanghui1986527@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```